﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using MAIN.Model.usersdb;
using MAIN.View;

namespace MAIN.ViewModel
{
    class UsersViewModel : BaseViewModel
    {

        DataContext db;
        Command addCommand;

        IEnumerable<User> users;

        public IEnumerable<User> Users
        {
            get { return users; }
            set
            {
                users = value;
                OnPropertyChanged(nameof(Users));
            }
        }

        private User selectedUser;
        public User SelectedUser
        {
            get
            {
                return selectedUser;
            }
            set
            {
                selectedUser = value;
                OnPropertyChanged(nameof(SelectedUser));
            }
        }

        private string selectedIndex;
        public string SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                selectedIndex = value;
                OnPropertyChanged(nameof(SelectedIndex));
            }
        }

        public UsersViewModel()
        {
            db = new DataContext();
            db.Users.Load();
            Users = db.Users.Local.ToBindingList();
        }

        public Command AddCommand
        {
            get
            {
                return addCommand ??
                  (addCommand = new Command((o) =>
                  {
                      //-----------VM-----------//
                      DialogWindowViewModel DialogVm = new DialogWindowViewModel(new User());
                      if (DialogVm.DialogResult == true)
                      {
                          User user = DialogVm.User;
                          db.Users.Add(user);
                          db.SaveChanges();
                      }
                  }));
            }
        }

        public ICommand Edit
        {
            get
            {
                  return new Command((obj) =>
                  {
                      if (SelectedUser == null) return;
                      // получаем выделенный объект
                      DialogWindowViewModel DialogVm = new DialogWindowViewModel(SelectedUser);
                      if (DialogVm.DialogResult == true)
                      {
                          SelectedUser = db.Users.Find(DialogVm.User.Id);
                          if (SelectedUser != null)
                          {
                              SelectedUser.Name = DialogVm.Name;
                              SelectedUser.Surname = DialogVm.Surname;
                              SelectedUser.Patronamic = DialogVm.Patronamic;
                              SelectedUser.RFID = DialogVm.RFID;
                              SelectedUser.Photo = DialogVm.Photo;

                              db.Entry(SelectedUser).State = EntityState.Modified;
                              db.SaveChanges();
                          }
                      }
                  });
            }
        }

        public ICommand Delete
        {
            get
            {
                return new Command((obj) =>
                {
                    if (SelectedUser != null)
                    {
                        // SelectedIndex = "-1";
                        User user = new User();
                        user = SelectedUser;
                        SelectedIndex = "-1";
                        //File.Delete(SelectedUser.Photo);
                        db.Users.Remove(user);
                        db.SaveChanges();
                    }
                });
            }
        }


        public ICommand Cancel
        {
            get
            {
                return new Command((obj) =>
                {
                    if (SelectedUser != null)
                    {
                        //SelectedIndex = "-1";
                        SelectedUser = null;
                    }
                });
            }
        }

    }
}
