﻿using MAIN.Model;
using System;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Collections.Generic;

namespace MAIN.ViewModel
{
    public class DeviceViewModel : BaseViewModel
    {
        public ObservableCollection<Device> DevicesList { get; set; }

        public void Update()
        {
            DevicesList = null;
            DevicesList = new ObservableCollection<Device>();
            if (Server.Devices.Count >= 0)
            {
                foreach (Device item in Server.Devices)
                {
                    DevicesList.Add(item);
                    OnPropertyChanged(nameof(DevicesList));
                }
            }
        }
        public DeviceViewModel()
        {
            Server.ListChanged += Server_ListChanged;       
        }

        private void Server_ListChanged()
        {
            Update();
        }

        private Device selectedDevice;
        public Device SelectedDevice
        {
            get
            {
                return selectedDevice;
            }
            set
            {
                selectedDevice = value;
                OnPropertyChanged(nameof(SelectedDevice));
            }
        }

        public ICommand Disconnect
        {
            get
            {
                return new Command((obj) =>
                {
                    try
                    {
                        if (selectedDevice != null)
                        {
                            if (DevicesList.Count == 1)
                            {
                                
                                DevicesList.Remove(SelectedDevice);
                                SelectedDevice.Client.Disconnect();
                            }
                            Server.RemoveConnection(SelectedDevice.Client.Id);
                            Update();
                            SelectedDevice = null;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                });
            }
        }
    }
}
