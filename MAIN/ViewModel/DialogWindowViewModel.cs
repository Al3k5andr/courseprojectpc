﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using MAIN.Model.usersdb;
using MAIN.View;
using GongSolutions.Wpf.DragDrop;
using System.Windows;
using System.IO;
using static System.Net.Mime.MediaTypeNames;
using System.Data.Entity;

namespace MAIN.ViewModel
{
    public class DialogWindowViewModel : BaseViewModel, IDataErrorInfo, IDropTarget
    {
        private User user;
        public User User
        {
            get { return user; }
            set
            {
                user = value;
                OnPropertyChanged(nameof(user));
            }
        }

        private DialogUserWindow dialogView;
        public DialogUserWindow DialogView
        {
            get { return dialogView; }
            set
            {
                dialogView = value;
                OnPropertyChanged(nameof(DialogView));
            }
        }

        //--------------------USERINF-----------------------/>
        public string Name
        {
            get { return User.Name; }
            set
            {
                User.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public string Surname
        {
            get { return User.Surname; }
            set
            {
                User.Surname = value;
                OnPropertyChanged(nameof(Surname));
            }
        }

        public string Patronamic
        {
            get { return User.Patronamic; }
            set
            {
                User.Patronamic = value;
                OnPropertyChanged(nameof(Patronamic));
            }
        }
        //--------------------KEYINF--------------------->
        public string RFID
        {
            get { return user.RFID; }
            set
            {
                User.RFID = value;
                OnPropertyChanged(nameof(RFID));
            }
        }

        public int AccessLevel
        {
            get { return User.AccessLevel; }
            set
            {
                User.AccessLevel = value;
                OnPropertyChanged(nameof(AccessLevel));
            }
        }
        //--------------------KEYINF------------------/>

        //--------------------USER ADDED INF----------------->
        public string PhoneNumber
        {
            get { return User.PhoneNumber; }
            set
            {
                User.PhoneNumber = value;
                OnPropertyChanged(nameof(PhoneNumber));
            }
        }

        public string Addr
        {
            get { return User.Addr; }
            set
            {
                User.Addr = value;
                OnPropertyChanged(nameof(Addr));
            }
        }
        //-------------------VALIDATION---------------------->

        public string Error
        {
            get { return null; }
        }
        Regex name_regex = new Regex(@"^[а-яА-ЯёЁa-zA-Z]{2,20}");
        Regex RFID_regex = new Regex(@"\d{10}");

        private bool validationResult = false;
        public bool ValidationResult
        {
            get { return validationResult; }
            set
            {
                validationResult = value;
                OnPropertyChanged(nameof(ValidationResult));
            }
        }

        static bool NameValidation;
        static bool SurnameValidation;
        static bool PatronamicValidation;
        static bool RFIDValidation;

        public void CheckValidation()
        {
            if (RFIDvalidator == true)
            {
                ValidationResult = (NameValidation == true && SurnameValidation == true && PatronamicValidation == true && RFIDValidation == true) ? true : false;
            }
            else
            {
                ValidationResult = (NameValidation == true && SurnameValidation == true && PatronamicValidation == true && RFIDValidation == false) ? true : false;
            }
        }

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                switch (columnName)
                {
                    case nameof(Name):
                        NameValidation = (Name != null && name_regex.IsMatch(Name)) ? true : false;
                        result = (NameValidation == false) ? "Incorrect Name" : result;
                        CheckValidation();
                        return result;
                    case nameof(Surname):
                        SurnameValidation = (Surname != null && name_regex.IsMatch(Surname)) ? true : false;
                        result = (SurnameValidation == false) ? "Incorrect Surname" : result;
                        CheckValidation();
                        return result;
                    case nameof(Patronamic):
                        PatronamicValidation = (Patronamic != null && name_regex.IsMatch(Patronamic)) ? true : false;
                        result = (PatronamicValidation == false) ? "Incorrect Patronamic" : result;
                        CheckValidation();
                        return result;
                    case nameof(RFID):
                        RFIDValidation = (RFIDvalidator == true && RFID != null && RFID_regex.IsMatch(RFID) && SearchRFID(RFID) == false) ? true : false;
                        //RFIDValidation = (RFIDvalidator == false) ? false : true;
                        result = (RFIDValidation == false) ? "Incorrect RFID" : result;
                        CheckValidation();
                        return result;
                }
                return result;
            }
        }


        private bool RFIDvalidator = false;
        public bool RFIDValidator
        {
            get { return RFIDvalidator; }
            set
            {
                RFIDvalidator = value;
                OnPropertyChanged(nameof(RFIDValidator));
            }
        }

        public bool SearchRFID(string RFID)
        {
            DataContext dataContext = new DataContext();
            dataContext = new DataContext();
            dataContext.Users.Load();
            var users = dataContext.Users.Where(u => u.RFID == RFID);
            return (users.Count() != 0) ? true : false;
        }

        //--------------------------------------------------/>
        public bool DialogResult = false;



        public DialogWindowViewModel()
        {
        }
        public DialogWindowViewModel(User user)
        {

            User = user;

            RFIDValidator = (User.RFID == null) ? true : false;


            if (User.Photo != null)
            {
                // File.Delete(standartPhotoPath);
                ImgPath = User.Photo;
                Photo = null;
                DragAnimation = "Transparent";
            }

            DialogView = new DialogUserWindow
            {
                DataContext = this
            };

            if (DialogView.ShowDialog() == true)
            {
                standartPhotoPath = $@"{Environment.CurrentDirectory}\Photos\{User.RFID}.png";

                //Photo = ImgPath;
                SavePicture();
                DialogResult = true;

                DialogView.Close();
            }
        }


        public void SavePicture()
        {
            if (File.Exists("temp.png"))
            {
                File.Delete("temp.png");
            }
            Photo = null;
            if (ImgPath == transparentPhoto && Photo == null)
            {
                Photo = null;
                DialogResult = true;
                DialogView.Close();
            }


            if (ImgPath != transparentPhoto && Photo == null)
            {
                if (ImgPath == standartPhotoPath)
                {
                    Photo = standartPhotoPath;
                    DialogResult = true;
                    DialogView.Close();
                }

                //DEBUG
                if (ImgPath != standartPhotoPath)
                {
                    File.Copy(ImgPath, "temp.png");
                    ImgPath = "temp.png";
                    try
                    {
                        if (File.Exists(standartPhotoPath))
                        {
                            File.Delete(standartPhotoPath);
                        }
                    }
                    catch
                    {

                    }
                    File.Copy("temp.png", standartPhotoPath);
                    File.Delete("temp.png");
                    Photo = standartPhotoPath;
                    DialogResult = true;
                    DialogView.Close();
                }
            }

        }

        //---------------------------------DRAG & DROP--------------------------------//
        private static string transparentPhoto = $@"{Environment.CurrentDirectory}\Photo\user.png";
        private static string tempPhoto = $@"{Environment.CurrentDirectory}\Photo\temp.png";
        private static string standartPhotoPath;

        private string imgPath = transparentPhoto;
        public string ImgPath
        {
            get { return imgPath; }
            set
            {
                imgPath = value;
                OnPropertyChanged(nameof(ImgPath));

            }
        }

        public string Photo
        {
            get { return User.Photo; }
            set
            {
                User.Photo = value;
                OnPropertyChanged(nameof(Photo));
            }
        }

        public ICommand DeleteImg
        {
            get
            {
                return new Command((obj) =>
                {
                    try
                    {
                        DragAnimation = "#ffffff";
                        ImgPath = transparentPhoto;
                        Photo = null;
                    }
                    catch { }
                });
            }
        }

        private string dragAnimation = "#ffffff";
        public string DragAnimation
        {
            get { return dragAnimation; }
            set
            {
                dragAnimation = value;
                OnPropertyChanged(nameof(DragAnimation));
            }
        }

        void IDropTarget.DragOver(IDropInfo dropInfo)
        {
            var dragFileList = ((DataObject)dropInfo.Data).GetFileDropList().Cast<string>();
            dropInfo.Effects = dragFileList.Any(item =>
            {
                var extension = Path.GetExtension(item);
                return extension != null && extension.Equals(".png");
            }) ? DragDropEffects.Copy : DragDropEffects.None;
        }

        void IDropTarget.Drop(IDropInfo dropInfo)
        {
            var dragFileList = ((DataObject)dropInfo.Data).GetFileDropList().Cast<string>();
            dropInfo.Effects = dragFileList.Any(item =>
            {
                var extension = Path.GetExtension(item);
                //-------------------------------
                ImgPath = Path.GetFullPath(item);
                DragAnimation = "Transparent";
                //-------------------------------
                return extension != null && extension.Equals(".png");
            }) ? DragDropEffects.Copy : DragDropEffects.None;
        }
    }
}
