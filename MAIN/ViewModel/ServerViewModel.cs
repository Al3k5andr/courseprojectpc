﻿using System;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Input;

using MAIN.Model;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace MAIN.ViewModel
{

    class ServerViewModel : BaseViewModel, IDataErrorInfo
    {
        private string ip;
        public string IP
        {
            get { return ip; }
            set
            {
                ip = value;
                OnPropertyChanged(nameof(IP));
            }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                OnPropertyChanged(nameof(Message));
            }
        }

        private string status = "OFFLINE";
        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                OnPropertyChanged(nameof(Status));
            }
        }

        //--------------VIEW--------------------//
        private string startStop = "START";
        public string StartStop
        {
            get { return startStop; }
            set
            {
                startStop = value;
                OnPropertyChanged(nameof(StartStop));
            }
        }

        public ServerViewModel()
        {
            IP = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString();
        }

        public ObservableCollection<string> LogList { get; set; }//Server Log
        public ObservableCollection<string> List = new ObservableCollection<string>(); //Temp _Log //'crutches && bicycles...'

        public ICommand StartServer
        {
            get
            {
                return new Command((obj) =>
                {
                    try
                    {
                        if (IP != null && ip_regex.IsMatch(IP))
                            Start(IP);
                    }
                    catch (Exception ex)
                    {
                        List.Add(ex.Message);
                        Update();
                    }
                });
            }
        }

        //'crutches && bicycles...'
        public void Update()
        {
            LogList = new ObservableCollection<string>();
            foreach (string item in List)
                LogList.Add(item);
            OnPropertyChanged(nameof(LogList));
        }

        public void Start(string ip)
        {
            if (StartStop == "START")
            {
                try
                {
                    Server.MessageChanged += Server_MessageChanged;
                    Server.ip = ip;
                    List.Add(Server.ip);
                    Update();
                    Status = "ONLINE";
                    //-------------
                    StartStop = "STOP";
                    //-------------
                    Task.Factory.StartNew(() =>
                    {
                        Server.Listen();
                    });
                }
                catch (Exception ex)
                {
                    Status = "OFFLINE";
                    Message = ex.Message;
                    Server.Shutdown();
                }
            }

            else
            {
                try
                {
                    Server.MessageChanged -= Server_MessageChanged;
                    Server.Shutdown();
                    StartStop = "START";
                    Status = "OFFLINE";
                    Server_MessageChanged("SERVER OFFLINE");
                }
                catch
                {

                }
            }
        }

        //'crutches && bicycles...'
        private void Server_MessageChanged(string message)
        {
            Message = DateTime.Now.ToLongTimeString() + " : " + message;
            List.Add(Message);
            Update();
        }

        //--------------VALIDATION----------------->

        private bool validationResult = false;
        public bool ValidationResult
        {
            get { return validationResult; }
            set
            {
                validationResult = value;
                OnPropertyChanged(nameof(ValidationResult));
            }
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == nameof(IP))
                {
                    ValidationResult = (ip_regex.IsMatch(IP) && IP != null) ? true : false;
                    result = (ValidationResult == true) ? string.Empty : "Incorrect Ip";
                }
                return result;
            }
        }
        Regex ip_regex = new Regex(@"(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}");
        //------------------------------------------/>
    }
}