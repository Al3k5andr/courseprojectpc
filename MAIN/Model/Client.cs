﻿using System;
using System.Text;
using System.Net.Sockets;
using MAIN.Model.usersdb;
using System.Data.Entity;
using System.Linq;
using System.IO;

namespace MAIN.Model
{
    public class Client
    {
        public string Id { get; private set; }
        public NetworkStream Stream { get; private set; }
        private TcpClient client;
        private string msg;
        public Device device;

        public delegate void Method();
        public static event Method Connected;


        public Client(TcpClient tcpClient)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            Server.AddConnection(this);
            device = new Device(this);
        }
        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                GetDeviceInformation();

                if (device.Name != null)
                {
                    Server.Devices.Add(device);
                    Connected(); //!!! EVENT
                }

                while (true)
                {
                    if (client.Connected)
                    {
                        try
                        {
                            AutoProcess();
                        }
                        catch
                        {
                            string message = String.Format($"{device.Name}: DISCONNECTED");
                            Server.ServerMSG = message;
                            break;
                        }
                    }
                    else
                    {
                        client.Close();
                        Server.RemoveConnection(Id);
                        Connected(); //!!! EVENT
                    }
                }
            }
            catch (Exception ex)
            {
                Server.ServerMSG = ex.Message;
            }
            finally
            {
                Server.RemoveConnection(this.Id);
                Disconnect();
            }
        }

        private string GetMessage()
        {
            byte[] data = new byte[64]; // буфер для получаемых данных
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);
            return builder.ToString();
        }

        public void GetClientMsg()
        {
            try
            {
                string message = GetMessage();
                try
                {
                    msg = message.Substring(0, 10);
                }
                catch
                {
                    msg = message;
                }
                Server.ServerMSG = device.Name + ":  " + msg;
            }
            catch (Exception ex)
            {
                //Server.Devices.Remove(this.device);
                Server.ServerMSG = device.Name + " ERRORE MESSAGE: "+ msg;
                //Connected(); //!!! EVENT
            }
        }

        public void SendToClientMsg(string message)
        {
            try
            {
                Server.SendMessage(message, this.Id);
            }
            catch (Exception ex)
            {
                Server.ServerMSG = ex.Message;
                Server.Devices.Remove(device);
                Connected(); //!!! EVENT
            }
        }
        public void SendToClientIMG(string imgPath)
        {
            try
            {
                if (imgPath != null)
                {
                    Server.SendImage(imgPath, this.Id);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Disconnect()
        {
            Server.Devices.Remove(device);
            Connected(); //!!! EVENT
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }

        public User SearchRFID(string RFID)
        {
            DataContext dataContext = new DataContext();
            dataContext = new DataContext();
            dataContext.Users.Load();
            var users = dataContext.Users.Where(u => u.RFID == RFID);
            if (users.Count() != 0)
            {
                User user = dataContext.Users.Where(u => u.RFID == RFID).First();
                return user;
            }
            else
            {
                return null;
            }
        }
        
        public void AutoProcess()
        {
            if (device.Name.Contains("Android"))
                PhoneProcess();
            if (device.Name.Contains("Arduino"))
                ArduinoProcess();
            else
            {
                PhoneProcess();
            }
        }

        public void PhoneProcess()
        {
            while (true)
            {
                GetClientMsg();

                if (msg != null)
                {

                    User u = SearchRFID(msg.Substring(0, 10));
                    if (u != null)
                    {
                        SendToClientMsg(u.Name + "-" + u.Surname + "-" + u.Patronamic);
                        if (GetMessage() == "ReadyToGet")
                        {

                            byte[] img = (u.Photo != null) ? File.ReadAllBytes(u.Photo) : File.ReadAllBytes("noPhoto.png");

                            SendToClientMsg(img.Length.ToString());
                            if (GetMessage() == "ReadyToGet")
                            {
                                if (u.Photo != null)
                                {
                                    SendToClientIMG(u.Photo);
                                }
                                else
                                {
                                    SendToClientIMG("noPhoto.png");
                                }
                            }   
                        }
                    }
                    else
                    {
                        SendToClientMsg("NO SUCH RFID IN DATABASE");
                    }

                }
            }
        }

        public void ArduinoProcess()
        {
            while (true)
            {
                GetClientMsg();
                //AccessLevel TODO
                if (SearchRFID(msg.Substring(0, 10)) != null)
                {
                    SendToClientMsg("OPEN");
                }
                else
                {
                    SendToClientMsg("CLOSE");
                }
            }
        }

        public void GetDeviceInformation()
        {
            try
            {
                string[] DeviceInformation = GetMessage().Split(new char[] { '-' });
                device.IP = DeviceInformation[0];
                device.Name = DeviceInformation[1];

                Server.ServerMSG = device.Name + "-" + device.IP + " CONNECTED";
            }
            catch (Exception ex)
            {
                Server.ServerMSG = ex.Message;
            }
            Connected(); //!!! EVENT
        }
    }
}
