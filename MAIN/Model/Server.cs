﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.ObjectModel;

namespace MAIN.Model
{
    public static class Server
    {
        public delegate void MethodContainer(string message);
        public static event MethodContainer MessageChanged;

        static TcpListener tcpListener;
        public static List<Client> clients = new List<Client>();

        public delegate void Method();
        public static event Method  ListChanged;

        public static string ip;
        public static List<Device> Devices = new List<Device>();
        
        private static string ServerMsg;
        public static string ServerMSG
        {
            get { return ServerMsg; }
            set
            {
                MessageChanged(value);
                ServerMsg = value;   
            }
        }

        private static void ClientObject_Connected()
        {
            ListChanged();
        }

        static internal void Listen()
        {
            try
            {
                tcpListener = new TcpListener(IPAddress.Parse(ip), 8888);
                tcpListener.Start();

                ServerMSG = "SERVER START WORK";
                ServerMSG = "WAIT FOR NEW CLIENTS...";

                while (true)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();

                    Client clientObject = new Client(tcpClient);

                    Client.Connected += ClientObject_Connected; //!!!!EVENT

                    Task.Factory.StartNew(() => clientObject.Process());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ServerMSG = ex.Message;
                ListChanged();
                Shutdown();
            }
        }

        public static void AddConnection(Client clientObject)
        {
            clients.Add(clientObject);
        }

        public static void SendMessage(string message, string id)
        {
            byte[] data = Encoding.UTF8.GetBytes(message);
            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].Id == id)
                {
                    clients[i].Stream.Write(data, 0, data.Length);
                    ServerMSG = "SERVER: " + message + "    (" + clients[i].device.Name +")";
                }
            }   
        }

        public static void SendImage(string fileName, string id)
        {
            byte[] img = File.ReadAllBytes(fileName);

            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].Id == id)
                {
                    clients[i].Stream.Write(img, 0, img.Length);
                    ServerMSG = "SERVER: " + fileName + "   bytes="+ img.Length.ToString() +"  Hash=" + img.GetHashCode()+ "(" + clients[i].device.Name + ")";
                }
            }
        }

        public static void Shutdown()
        {
            tcpListener.Stop();

            for (int i = 0; i < clients.Count; i++)
            {
                clients[i].Disconnect();
            }
        }

        public static void RemoveConnection(string id)
        {
            
            Client client = clients.FirstOrDefault(c => c.Id == id);
            ServerMSG = "SERVER: " + client.device.Name + " DISCONNECTED";
            Devices.Remove(client.device);
            client.Disconnect();
            ListChanged();

            if (client != null)
                clients.Remove(client);
        }
    }
}
