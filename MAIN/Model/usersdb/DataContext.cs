﻿using System.Data.Entity;

namespace MAIN.Model.usersdb
{
    class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
                
        }
        public DbSet<User> Users { get; set; }  
    }
}
