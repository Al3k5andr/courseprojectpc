﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAIN.ViewModel;

namespace MAIN.Model.usersdb
{
    public class User  :   BaseViewModel
    {
        public int Id { get; set; }
        private string name;
        private string surname;
        private string patronamic;

        private string rfid;
        private int accesslevel;

        private string photo;
        private string phonenumber;
        private string addr;

        //--------------------USERINF----------------------->

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                OnPropertyChanged(nameof(Surname));
            }
        }
        public string Patronamic
        {
            get { return patronamic; }
            set
            {
                patronamic = value;
                OnPropertyChanged(nameof(Patronamic));
            }
        }

        //--------------------USERINF-----------------------/>



        //--------------------KEYINF--------------------->
        public string RFID
        {
            get { return rfid; }
            set
            {
                rfid = value;
                OnPropertyChanged(nameof(RFID));
            }
        }
        public int AccessLevel
        {
            get { return accesslevel; }
            set
            {
                accesslevel = value;
                OnPropertyChanged(nameof(AccessLevel));
            }
        }

        //--------------------KEYINF------------------/>

        //--------------------USER ADDED INF----------------->

        public string Photo
        {
            get { return photo; }
            set
            {
                photo = value;
                OnPropertyChanged(nameof(Photo));
            }
        }
        public string PhoneNumber
        {
            get { return phonenumber; }
            set
            {
                phonenumber = value;
                OnPropertyChanged(nameof(PhoneNumber));
            }
        }
        public string Addr
        {
            get { return addr; }
            set
            {
                addr = value;
                OnPropertyChanged(nameof(Addr));
            }
        }

        //--------------------USER ADDED INF-----------------/>
    }
}
