﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MAIN.Model
{
    public class Device
    {
        private string name;
        private string ip;
        private string status;
        private string image;
        private Client client;
        public Client Client
        {
            get { return client; }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
            }
        }
        public string IP
        {
            get { return ip; }
            set
            {
                ip = value;
            }
        }
        public string Status
        {
            get { return status; }
            set
            {
                status = value;
            }
        }
        
        public string Image
        {
            get { return image; }
            set
            {
                image = value;
            }
        }


        public Device(Client client)
        {
            this.client = client;
        }
    }
}
